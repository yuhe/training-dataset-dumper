export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

asetup Athena,master,2021-08-01T2101

# add h5ls
. ${BASH_SOURCE[0]%/*}/setup/add-h5-tools.sh
