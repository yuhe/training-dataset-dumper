FROM placeholder-will-be-replaced-with-CI-variable

## ensure locale is set during build
ENV LANG C.UTF-8

COPY . /btagging/training-dataset-dumper
WORKDIR /btagging

USER root

RUN yum -y install libtiff && yum clean all

RUN cd /btagging && \
    mkdir /btagging/build && \
    cd build && \
    source /release_setup.sh && \
    cmake ../training-dataset-dumper && \
    make

CMD source /release_setup.sh && source  /btagging/build/x*/setup.sh && /bin/bash
