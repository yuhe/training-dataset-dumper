variables:
  # we can use the cern DOCKER_CACHE to avoid overloading dockerhub
  # see here: https://clouddocs.web.cern.ch/containers/tutorials/registry.html#dockerhub-cache
  #
  DOCKER_CACHE: registry.cern.ch/docker.io
  BUILD_IMAGE: $DOCKER_CACHE/atlas/analysisbase:22.2.42
  ATH_IMAGE: &ath-image $DOCKER_CACHE/atlas/athanalysis:22.2.42
  DATA_URL: https://dguest-ci.web.cern.ch/dguest-ci/ci/ftag/dumper

image: $BUILD_IMAGE



##
## Various steps to be followed by the build
##
stages:
  - build
  - run
  - cleanup
  - imagebuild

##
## Common setup for all jobs
##
before_script:
  - set +e
  - source ~/release_setup.sh
  - if [[ -d build ]]; then source build/**/setup.sh; fi

##
## first compile the code
##
compile:
   stage: build
   script:
     - mkdir -p build && cd build
     - cmake .. | tee cmake.log
     - make clean #make sure we don't have residual compilation results
     - make -j $(nproc) 2>&1 | tee -a cmake.log  #dump the log files
   artifacts:
     paths:
       - build

##
## another version with AthAnalysis
##
compile_athanalysis:
  extends: compile
  image: *ath-image


###################################
## run tests
###################################
test_pflow:
  stage: run
  needs: [compile]
  script:
    - test-dumper pflow

test_trackjets:
  stage: run
  needs: [compile]
  script:
    - test-dumper trackjets

test_gnn:
  stage: run
  needs: [compile]
  script:
    - test-dumper gnn

test_eventloop:
  stage: run
  needs: [compile]
  script:
    - test-dumper eventloop

test_component_accumulator:
  image: *ath-image
  stage: run
  needs: [compile_athanalysis]
  script:
    - test-dumper eventloop

###################################
## build images
###################################
build_img_latest:
  needs:
    - compile
  stage: imagebuild
  allow_failure: true
  image: gitlab-registry.cern.ch/ci-tools/docker-image-builder:no_kaniko
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    FROM: $BUILD_IMAGE
    TO: $CI_REGISTRY_IMAGE:latest
  tags:
    - docker-image-build
  script:
    - ignore
  only:
    - master


build_img_tag:
  needs:
    - compile
  stage: imagebuild
  allow_failure: true
  image: gitlab-registry.cern.ch/ci-tools/docker-image-builder:no_kaniko
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    FROM: $BUILD_IMAGE
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags:
    - docker-image-build
  script:
    - ignore
  only:
    - tags
