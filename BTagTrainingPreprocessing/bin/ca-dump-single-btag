#!/usr/bin/env python3

"""
Dump some ftag info using the "new" configuration
"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from GaudiKernel.Configurable import DEBUG, INFO

from argparse import ArgumentParser

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='test.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    return parser.parse_args()

def getSingleBTagConfig(config, output):
    ca = ComponentAccumulator()
    btagAlg = CompFactory.SingleBTagAlg('DatasetDumper')
    btagAlg.outputFile = output
    btagAlg.configFileName = config

    ca.addEventAlgo(btagAlg)
    return ca

def run():
    args = get_args()

    cfgFlags.Input.Files = args.input_files
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = DEBUG if args.debug else INFO

    cfgFlags.lock()

    ca = getConfig(cfgFlags)

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(getSingleBTagConfig(args.config_file, args.output))

    ca.run()

if __name__ == '__main__':
    run()


