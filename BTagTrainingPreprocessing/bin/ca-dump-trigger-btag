#!/usr/bin/env python3

"""
Dump some ftag trigger info!

Input files can be separated by spaces or commas, which is useful to
deal with prun.
"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

# works in full athena for now
from TrigDecisionTool.TrigDecisionToolConfig import getTrigDecisionTool

from AthenaConfiguration.ComponentFactory import CompFactory
from GaudiKernel.Configurable import DEBUG, INFO, VERBOSE

from argparse import ArgumentParser
from itertools import chain
import sys
from math import inf

def get_args():
    default_chain = 'HLT_j20_0eta290_020jvt_pf_ftf_boffperf_L1J15'
    dh = dict(help='(default: %(default)s)')
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-t','--threads', type=int, default=0)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-n','--chain', default=default_chain, **dh)
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-o','--output', default='output.h5', **dh)
    parser.add_argument('-i','--event-print-interval', type=int,
                        default=100, **dh)
    return parser.parse_args()

def getLabelingAlgAndJetLabelTool(cfgFlags):
    label_tools = []
    for ptype in ['BHadrons','CHadrons','Taus']:
        tool = CompFactory.CopyFlavorLabelTruthParticles(f'{ptype}Builder')
        tool.ParticleType = f'{ptype}Final'
        tool.OutputName = f'TruthLabel{ptype}Final'
        tool.OutputLevel = cfgFlags.Exec.OutputLevel
        label_tools.append(tool)
    labelCollectionBuilder = CompFactory.JetAlgorithm("LabelBuilderAlg")
    labelCollectionBuilder.Tools = label_tools
    labelCollectionBuilder.OutputLevel = cfgFlags.Exec.OutputLevel

    dRTool = CompFactory.ParticleJetDeltaRLabelTool(
        "jetdrlabeler",
        LabelName = "HadronConeExclTruthLabelID",
        DoubleLabelName = "HadronConeExclExtendedTruthLabelID",
        BLabelName = "ConeExclBHadronsFinal",
        CLabelName = "ConeExclCHadronsFinal",
        TauLabelName = "ConeExclTausFinal",
        BParticleCollection = "TruthLabelBHadronsFinal",
        CParticleCollection = "TruthLabelCHadronsFinal",
        TauParticleCollection = "TruthLabelTausFinal",
        PartPtMin = 5000.0,
        JetPtMin =     0.0,
        DRMax = 0.3,
        MatchMode = "MinDR",
        OutputLevel = cfgFlags.Exec.OutputLevel
    )
    return labelCollectionBuilder, dRTool

def getFSTrackAssociationAlgs(
        cfgFlags, jc, bc, tpc='HLT_IDTrack_FS_FTF', an='FSTracks'):
    acc = ComponentAccumulator()
    Associator = CompFactory.JetParticleShrinkingConeAssociation
    trackOnJetDecorator = f'{jc}.{an}ForBTagging'
    # the formula here is R = p1 + exp(p2 + p3 * pt)
    assoc_tool = Associator(
        'FSTrackConeTool',
        coneSizeFitPar1=0.5,
        coneSizeFitPar2=-inf,   # exp(-inf) -> 0
        coneSizeFitPar3=0,      # fixed cone
        JetContainer=jc,
        InputParticleContainer=tpc,
        OutputDecoration=trackOnJetDecorator.split('.')[-1],
    )
    assoc_alg = CompFactory.JetDecorationAlg(
        'FSTrackAssociationAlg',
        JetContainer=jc,
        Decorators=[assoc_tool])
    acc.addEventAlgo(assoc_alg)

    Copier = CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg
    copier = Copier(
        'FSTrackCopier',
        jetTracks=trackOnJetDecorator,
        btagTracks=f'{bc}.{an}',
        jetLinkName=f'{bc}.jetLink'
    )
    acc.addEventAlgo(copier)
    return acc

# We do imports in this function because it's expensive to set up and
# only works in Athena. It should only be required for the btagIp_*
# variables.
def getTrackAugmentation(
        cfgFlags,
        tpc='HLT_IDTrack_FS_FTF',
        pvc='HLT_IDVertex_FS'):
    try:
        from BTagging.BTagTrackAugmenterAlgConfig import (
            BTagTrackAugmenterAlgCfg as TrackAugCfg)
        return TrackAugCfg(
            cfgFlags,
            TrackCollection=tpc,
            PrimaryVertexCollectionName=pvc)
    except ModuleNotFoundError as err:
        print(f'WARNING: problem setting up track augmentation: {err}')
        return ComponentAccumulator()


def trigDatasetDumper(cfgFlags, chain, config_file, output_file):
    ca = ComponentAccumulator()

    # We have two ways to set up the trigger decision tool, because it
    # works differently in full Athena and in AthAnalysis.
    try:
        # First the Athena way. This sets up the trigger decision tool
        # the way Tim Martin recommends.
        trigDecTool = getTrigDecisionTool(cfgFlags).getPrimary()
    except RuntimeError:
        # This is the fallback way since the above function fails in
        # AthAnalysis. As of 2021.03.25 this version only works for
        # the first file, then crashes in one or another terrible way.
        trigDecTool = CompFactory.Trig.TrigDecisionTool('TrigDecisionTool')
        trigDecTool.NavigationFormat = "TrigComposite" # Run 3 style

    # Jet labeling
    labelAlg, dRTool = getLabelingAlgAndJetLabelTool(cfgFlags)
    ca.addEventAlgo(labelAlg)

    temp_jets, temp_btag = 'tempJets', 'tempBtag'

    # Local component to move trigger elements into collections so we
    # can access them inside offline code
    jetGetter = CompFactory.TriggerJetGetterAlg(
        'TriggerJetGetterAlg')
    jetGetter.triggerDecisionTool = trigDecTool
    jetGetter.OutputLevel = cfgFlags.Exec.OutputLevel
    jetGetter.bJetChain = chain
    jetGetter.outputJets = temp_jets
    jetGetter.outputBTag = temp_btag
    jetGetter.jetModifiers = [dRTool]
    ca.addEventAlgo(jetGetter)

    # match to offline jets, pull out some info
    matcher = CompFactory.TriggerBTagMatcherAlg('matcher')
    matcher.offlineBtagKey = 'BTagging_AntiKt4EMTopo'
    matcher.triggerBtagKey = temp_btag
    matcher.floatsToCopy = {
        f'DL1r_p{x}':f'OfflineMatchedDL1r_p{x}' for x in 'bcu'}
    matcher.offlineJetKey = 'AntiKt4EMTopoJets'
    matcher.triggerJetKey = temp_jets
    truth_labels = [
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
    ]
    matcher.jetIntsToCopy = {
        x:f'OfflineMatched{x}' for x in truth_labels}
    ca.addEventAlgo(matcher)

    # associate fullscan tracks to the jets
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca.merge(getTrackAugmentation(
        cfgFlags,
        tpc=fs_tracks,
        pvc=fs_vertices,
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        TrackContainer=fs_tracks,
        PrimaryVertexContainer=fs_vertices
    ))
    ca.merge(getFSTrackAssociationAlgs(cfgFlags, temp_jets, temp_btag))

    btagAlg = CompFactory.SingleBTagAlg('DatasetDumper')
    btagAlg.outputFile = output_file
    btagAlg.configFileName = config_file
    btagAlg.OutputLevel = cfgFlags.Exec.OutputLevel
    ca.addEventAlgo(btagAlg)
    return ca


def run():
    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags

    cfgFlags.Input.Files = list(
        chain.from_iterable(f.split(',') for f in args.input_files))

    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events

    cfgFlags.Concurrency.NumThreads = args.threads
    if args.threads:
        cfgFlags.Concurrency.NumConcurrentEvents = args.threads
    cfgFlags.Exec.OutputLevel = INFO
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG
        cfgFlags.Exec.DebugStage = 'exec'

    cfgFlags.lock()

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = getConfig(cfgFlags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # This is also needed for TDT
    ca.merge(MetaDataSvcCfg(cfgFlags))

    # Needed to read anything from a file
    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(
        trigDatasetDumper(
            cfgFlags,
            chain=args.chain,
            config_file=args.config_file,
            output_file=args.output,
        )
    )


    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
