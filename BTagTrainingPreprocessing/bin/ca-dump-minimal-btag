#!/usr/bin/env python3

"""
Dump some ftag info using the "new" configuration
"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg
)

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from GaudiKernel.Configurable import DEBUG, INFO

from argparse import ArgumentParser

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-i','--event-print-interval', type=int, default=100)
    return parser.parse_args()

def getSingleBTagConfig(flags, config, output):
    ca = ComponentAccumulator()

    btag_name = 'TestBTagging'
    jet_name = 'AntiKt4EMPFlowJets'
    builder = CompFactory.FlavorTagDiscriminants.BTaggingBuilderAlg(
        'btagbuilder',
        jetName=jet_name,
        btaggingName=btag_name)
    ca.addEventAlgo(builder)

    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        'PoorMansIpAugmenterAlg'))

    ca.addEventAlgo(CompFactory.JetToBTagLinkerAlg(
        'jetToBtag',
        oldLink=f'{btag_name}.jetLink',
        newLink=f'{jet_name}.btaggingLink'
    ))

    track_decor = "TracksForMinimalTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection="InDetTrackParticles",
        OutputParticleDecoration=track_decor
    ))

    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg(
            'copy_alg',
            jetTracks=f'{jet_name}.{track_decor}',
            btagTracks=f'{btag_name}.BTagTrackToJetAssociator',
            jetLinkName=f'{btag_name}.jetLink'
        )
    )

    btagAlg = CompFactory.SingleBTagAlg('DatasetDumper')
    btagAlg.outputFile = output
    btagAlg.configFileName = config

    ca.addEventAlgo(btagAlg)
    return ca

def run():
    args = get_args()

    cfgFlags.Input.Files = args.input_files
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = DEBUG if args.debug else INFO

    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(getSingleBTagConfig(cfgFlags, args.config_file, args.output))

    ca.run()

if __name__ == '__main__':
    run()


