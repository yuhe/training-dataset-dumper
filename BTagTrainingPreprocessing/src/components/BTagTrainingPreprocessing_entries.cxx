#include "src/SingleBTagAlg.h"
#include "src/TriggerJetGetterAlg.h"
#include "src/TriggerBTagMatcherAlg.h"

DECLARE_COMPONENT(SingleBTagAlg)
DECLARE_COMPONENT(TriggerJetGetterAlg)
DECLARE_COMPONENT(TriggerBTagMatcherAlg)
