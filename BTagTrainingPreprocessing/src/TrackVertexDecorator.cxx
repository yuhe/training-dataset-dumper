#include "TrackVertexDecorator.hh"


// the constructor just builds the decorator
TrackVertexDecorator::TrackVertexDecorator(const std::string& prefix):
  m_track_sv1_idx(prefix + "SV1VertexIndex"),
  m_track_jf_idx(prefix + "JFVertexIndex"),
  m_SV1VxAccessor("SV1_vertices"),
  m_jfVxAccessor("JetFitter_JFvertices")
{
}

bool particleInCollection( const xAOD::TrackParticle& track, std::vector< ElementLink< xAOD::TrackParticleContainer>> track_collection ) {
  for ( auto test_track : track_collection ) {
    if (&track == *test_track) return true;
  }
  return false;
}

int get_sv1_index( const xAOD::TrackParticle& track, std::vector<ElementLink<xAOD::VertexContainer>>& sv1_vertices ) {
  int track_jf_idx = -2;
  for (unsigned int sv1_idx = 0; sv1_idx < sv1_vertices.size(); sv1_idx++) { 
    if (!sv1_vertices.at(sv1_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::Vertex *tmpVertex = *(sv1_vertices.at(sv1_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->trackParticleLinks();

    // if the track was used, save the index
    if ( particleInCollection(track, tracks_in_vertices)) {
      track_jf_idx = sv1_idx;
      break;
    }
  }
  return track_jf_idx;
}

int get_jf_index( const xAOD::TrackParticle& track, std::vector<ElementLink<xAOD::BTagVertexContainer>>& jf_vertices ) {
  int track_jf_idx = -2;
  for (unsigned int jf_idx = 0; jf_idx < jf_vertices.size(); jf_idx++) { 
    if (!jf_vertices.at(jf_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::BTagVertex *tmpVertex = *(jf_vertices.at(jf_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->track_links();

    // if the track was used, save the index
    if ( particleInCollection(track, tracks_in_vertices)) {
      track_jf_idx = jf_idx;
      break;
    }
  }
  return track_jf_idx;
}

// this call actually does the work on the track
void TrackVertexDecorator::decorate(const xAOD::TrackParticle& track, const xAOD::Jet& jet) const {

  // get vertex collections
  const xAOD::BTagging* btag = xAOD::BTaggingUtilities::getBTagging(jet);
  std::vector<ElementLink<xAOD::VertexContainer>> sv1_vertices = m_SV1VxAccessor(*btag);
  std::vector<ElementLink<xAOD::BTagVertexContainer>> jf_vertices = m_jfVxAccessor(*btag);

  // get index of vertex associated to this track
  int track_sv1_idx = get_sv1_index(track, sv1_vertices);
  int track_jf_idx = get_jf_index(track, jf_vertices);

  // store index
  m_track_sv1_idx(track) = track_sv1_idx;
  m_track_jf_idx(track)  = track_jf_idx;
}
