#include "SingleBTagAlg.h"

#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "processSingleBTagEvent.hh"

#include "H5Cpp.h"

#include "AsgDataHandles/ReadHandle.h"

SingleBTagAlg::SingleBTagAlg(const std::string& name,
                             ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator),
  m_output(nullptr),
  m_tools(nullptr)
{
  declareProperty("outputFile", m_output_file);
  declareProperty("configFileName", m_config_file_name);
  declareProperty("metadataFileName",
                  m_metadata_file_name = "userJobMetadata.json");
}

SingleBTagAlg::~SingleBTagAlg() {
}

  // these are the functions inherited from Algorithm
StatusCode SingleBTagAlg::initialize () {
  auto cfg = get_singlebtag_config(m_config_file_name);
  m_config.reset(new SingleBTagConfig(cfg));
  m_output.reset(new H5::H5File(m_output_file, H5F_ACC_TRUNC));
  m_tools.reset(new SingleBTagTools(*m_config, *m_output));

  m_jetKey = cfg.jet_collection;
  ATH_CHECK(m_jetKey.initialize());
  ATH_MSG_DEBUG("Initialized SingleBTagAlg");

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  const auto& jobcfg = *m_config;
  auto& tools = *m_tools;

  // most of the real work happens in this function
  processSingleBTagEvent(event, jobcfg, tools);

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::finalize () {

  if (m_metadata_file_name.size() > 0) {
    writeTruthCorruptionCounts(m_tools->truth_counts, m_metadata_file_name);
  }

  return StatusCode::SUCCESS;
}
