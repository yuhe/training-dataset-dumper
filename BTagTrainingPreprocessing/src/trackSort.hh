#ifndef TRACKSORT_H
#define TRACKSORT_H

#include "TrackSortOrder.hh"

#include "xAODTracking/TrackParticleFwd.h"

typedef bool (*TrackSort)(const xAOD::TrackParticle* t1,
                          const xAOD::TrackParticle* t2);

TrackSort trackSort(TrackSortOrder order);

#endif
