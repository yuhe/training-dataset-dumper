#ifndef SINGLE_BTAG_OPTIONS_HH
#define SINGLE_BTAG_OPTIONS_HH

#include <vector>
#include <string>

struct SingleTagIOOpts
{
  std::vector<std::string> in;
  std::string out;
  unsigned long long max_events;
  std::string config_file_name;
};

SingleTagIOOpts get_single_tag_io_opts(int argc, char* argv[]);


#endif
